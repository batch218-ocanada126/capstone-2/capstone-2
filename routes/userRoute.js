

const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

//start ofs42 capstone2
router.post("/register", (req, res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.get("/details/:id", (req, res) => {
	userController.getUser(req.params.id).then(resultFromController => res.send(resultFromController));
});
//end of s42 capstone2



router.post("/order", auth.verify, (req, res) =>{

	let data = {
		productId: req.body.productId,
		userId: auth.decode(req.headers.authoriztion).id
	}

	userController.order(data).then(resultFromController =>{
		res.send(resultFromController)
	});
});



module.exports = router;