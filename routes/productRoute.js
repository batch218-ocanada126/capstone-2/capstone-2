

const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


// Start of s43 capstone2
router.post("/createProduct", auth.verify, (req, res) =>{

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});


router.get("/activeProduct", (req, res) =>{
	productController.getActiveProduct().then(resultFromController => res.send(resultFromController));
});

// End of s43 capstone2


//Start of s44 capstone2

//all user
router.get("/:productId", (req, res) =>{

	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
});

//admin only
router.patch("/:productId", auth.verify, (req, res) =>{

	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params.productId, newData).then(resultFromController => res.send(resultFromController));
});

//admin only
router.patch("/:productId/archive", auth.verify, (req, res) =>{

	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(req.params.productId, newData).then(resultFromController => res.send(resultFromController)
	);
});


//End of s44 capstone2

module.exports = router;

