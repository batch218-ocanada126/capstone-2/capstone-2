

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Username is required"],
		unique: true	
	},
	email : {
		type : String,
		required : [true, "Email is required"],
		unique: true
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
		
	},
	orders : [
		{
			products : [
					{
						productName : {
							type: String,
							required: true
						},
						quantity : {
							type : Number,
							default : 1
						}
					}

				],
			totalAmount : {
				type : Number,
				required: true
			},
			purchasedOn : {
				type : Date,
				default : new Date()	
			}
			
		}
	]
	}
);



module.exports = mongoose.model("User", userSchema);	