



const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	title : {
		type : String,
		required: [true, "Name Product is required"],
		unique: true
	},
	description:{
		type: String,
		required: [true, "Description is required"]
	},
	size : {
		type : String
	},
	color : {
		type : String
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orderer : [
		{
			userId: {
				type: String,
				required: true
			}	
		}		
	]
});




module.exports = mongoose.model("Product", productSchema);