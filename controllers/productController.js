

const mongoose = require("mongoose");
const Product = require("../models/product.js");

//start of s43 capstone2
module.exports.createProduct = (data) =>{
	console.log(data.isAdmin)


	if(data.isAdmin){
		let newProduct = new Product({
			title: data.product.title,
			description: data.product.description,
			size: data.product.size,
			color: data.product.color,
			price: data.product.price,
			isActive: data.product.isActive
		});

		return newProduct.save().then((newProduct, error) =>{
			if(error){
				return error;
			}

			return newProduct
		})

	};

	let message = Promise.resolve('User must be ADMIN to create new product')

	return message.then((value) =>{
		return {value}
	})

};


module.exports.getActiveProduct = () =>{
	return Product.find({isActive:true}).then(result =>{
		return result;
	})
};
//end of s43 capstone2




//Start of s44 capstone2

// all Users
module.exports.getProduct = (productId) =>{

	return Product.findById(productId).then(result =>{
		return result;
	});
};


//admin only
module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin === true){
		//update code block
		return Product.findByIdAndUpdate(productId ,
		{
			title: newData.product.title,
			description: newData.product.description,
			size: newData.product.size,
			color: newData.product.color,
			price: newData.product.price
			//isActive: newData.product.isActive

		}
		).then((updatedProduct, error) =>{
			if(error){
				return false;
			}
			return newData;
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to update Product')
		return message.then((value) => {return value})
	};
};


//admin only
module.exports.archiveProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId , {
				isActive: newData.product.isActive
			})

		.then((archivedProduct, error) =>{
			if(error){
				return false;
				}

			return {
				message : "Product archived successfully!"
			}		
		});
	}
	else{
		let message = Promise.resolve('User must be ADMIN to archive Product')
		return message.then((value) => {return value})
	};
};

//End of s44 capstone2