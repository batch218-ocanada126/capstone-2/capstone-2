

const User = require("../models/user.js");
const Product = require("../models/product.js");


const bcrypt = require("bcrypt");
const auth = require("../auth.js");


//start s42 capstone2
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({

		username: reqBody.username,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
		
	})

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		return "You are now REGISTERED";
	})


};


module.exports.loginUser = (reqBody) =>{
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return {
				alert : "Invalid email or password!"
			}
			
		}
		else{
			// compareSync is bcrypt function to compare a unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password do not match
				return false;
				// return "Incorrect password"
			}
		}
	})

};


module.exports.getUser = (userId) => {
	return User.findById(userId).then((details, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			//details.password = "";
			return details;
		}
	})
}

//end of s42 capstone2



// start of s45 capstone2

module.exports.order = async (data) => {

	let isUserOrdered = await User.findById(data.userId).then(user => {

		user.orders.push({productId : data.productId});

		return user.save().then((user, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			};
		});
	});


	let isProductOrdered = await Product.findById(data.productId).then(product => {

		product.orderer.push({userId : data.userId});

		return product.save().the((product, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			};
		});
	});


	if(isUserOrdered && isProductOrdered){
		return true;
	}
	else{
		return false;
	}
}